
Feature: Usuario Dictaminador requiere marcar como cancelado un siniestro
  
  Scenario: Dictaminar Cancelado un Siniestro
  
  Given Usuario Dictaminador, cancel 
    When Accedo al sistema Claims, cancel
    Then Selecciono el siniestro a Dictaminar, cancel 
    Then Se muestra el detalle del siniestro y el boton dictaminar, cancel
    When Doy clic en el boton Dictaminar, cancel
    Then Selecciono la opcion Cancelar, cancel
    Then Se muestra el mensaje de confirmacion de cancelacion, cancel
    When Doy clic en el boton Continuar, cancel
    Then Se muestra el mensaje Dictaminacion Exitosa, cancel 
  
