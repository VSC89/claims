
Feature: Usuario Dictaminador requiere marcar como rechazado un siniestro

  Scenario: Dictaminar Rechazado un Siniestro
  
    Given Usuario Dictaminador, rejection 
    When Accedo al sistema Claims, rejection
    Then Selecciono el siniestro a Dictaminar, rejection 
    Then Se muestra el detalle del siniestro y el boton dictaminar, rejection
    When Doy clic en el boton Dictaminar, rejection
    Then Selecciono la opcion rechazar, rejection
    Then Se muestran los motivos de rechazo, rejection
    When Doy clic en el boton Continuar, rejection
    Then Se muestra el mensaje Dictaminacion Exitosa, rejection