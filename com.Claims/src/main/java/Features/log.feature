Feature: Acceder al sistema de siniestros mediante mi Smart Pass

Scenario: Ser identificado en el sistema y poder realizar actividades de acuerdo a mi rol

	Given Usuario Supervisor de la mesa de control
	
	When Ingreso la URL del sistema de siniestros
	
	Then Se me debe mostrar un boton para inicio de sesion
	
	And Presiono el boton de inicio de sesion 

	Then El usuario accede correctamente al sistema Claims
	
	And Presiono el boton Ingresar 

	Then Se muestra la pantalla Home
