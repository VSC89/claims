
Feature: Usuario Dictaminador requiere marcar como pendiente un siniestro
  
Scenario: Dictaminar Pendiente un Siniestro

    Given Usuario Dictaminador
    When Accedo al sistema Claims
    Then Selecciono el siniestro a Dictaminar
    Then Se muestra el detalle del siniestro y el boton dictaminar
    When Doy clic en el boton Dictaminar
    Then Selecciono la opcion pendiente
    Then Se muestran los motivos de pendiente
    When Doy clic en el boton Continuar
    Then Se muestra el mensaje Dictaminacion Exitosa
    
    
    
