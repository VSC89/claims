package StepDefinition;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class HitPimus_Mesa_Control {
	
	private ChromeDriver driver = Hooks.getDriver();
	
	@Given("^Analisra de mesa de control, hp$")
	public void analisra_de_mesa_de_control_hp() throws Throwable {
		
		WebElement botonIngresarSmart = driver.findElement(By.xpath("//input[@class='btn btn-primary rounded-pill mx-auto d-block']"));

		botonIngresarSmart.click();
		Thread.sleep(1000);

		String perfilAnalistaMesa = "VALIDITY_REVIEWER";

		WebElement selectPerfil = driver.findElement(By.xpath("//select[@class='form-control']"));
		selectPerfil.click();

		Select listaPerfiles = new Select(selectPerfil);
		listaPerfiles.selectByVisibleText(perfilAnalistaMesa);

	 
	 
	}

	@When("^Accedo al sistema Claims, hp$")
	public void accedo_al_sistema_Claims_hp() throws Throwable {
		
		Thread.sleep(1000);
		WebElement imageCardif = driver.findElement(By.xpath("//img[@class='pt-4 pb-4 pl-2 pr-2']"));

		Assert.assertTrue("Accedi� correctamente al Sistema Claims", imageCardif.isDisplayed());

	 
	}

	@Then("^Selecciono el siniestro a validar en PIMUS, hp$")
	public void selecciono_el_siniestro_a_validar_en_PIMUS_hp() throws Throwable {
		
		WebElement checkBoxSiniestro = driver.findElement(By.xpath("/html/body/app-root/app-claims-page/div/app-validation-pimus/app-table/table/tbody/tr[1]/td[1]/input"));
		checkBoxSiniestro.click();
		Thread.sleep(1000);
	 
	}

	@Then("^Se muestra el boton Hit PIMUS, hp$")
	public void se_muestra_el_boton_Hit_PIMUS_hp() throws Throwable {
		
		Thread.sleep(1000);
		WebElement botonHitPimus = driver.findElement(By.xpath("//button[contains(text(), 'Hit PIMUS')]"));

		Assert.assertTrue("Se mostr� correctamente el Bot�n Hit PIMUS", botonHitPimus.isDisplayed());

	 
	}

	@When("^Doy clic en el boton Hit PIMUS, hp$")
	public void doy_clic_en_el_boton_Hit_PIMUS_hp() throws Throwable {
		
		WebElement botonHitPimus = driver.findElement(By.xpath("//button[contains(text(), 'Hit PIMUS')]"));
		botonHitPimus.click();
		Thread.sleep(1000);
	 
	}

	@Then("^Se muestra el mensaje Se marcaran los siguientes como encontrados en PIMUS, hp$")
	public void se_muestra_el_mensaje_Se_marcar_n_los_siguientes_como_encontrados_en_PIMUS_hp() throws Throwable {
	 
			WebElement mensajeConfirmacion = driver.findElement(By.xpath("//div[@class='py-3']"));
		    Assert.assertTrue("Se mostro� correctamente el mensaje de confirmaci�n", mensajeConfirmacion.isDisplayed());		   
		
	}

	@When("^Doy clic en boton Continuar, hp$")
	public void doy_clic_en_boton_Continuar_hp() throws Throwable {
		
		WebElement botonContinuar = driver.findElement(By.xpath("//button[@class='btn btn-primary rounded-pill px-4 mr-3']"));
		botonContinuar.click();
		Thread.sleep(1000);			 
	 
	}

	@Then("^Se muestra el mensaje de Actualizacion Exitosa, hp$")
	public void se_muestra_el_mensaje_de_Actualizacion_Exitosa_hp() throws Throwable {
	 
		 WebElement mensajeDictaminacionExitosa = driver.findElement(By.xpath("//div[@role='alert']"));		
		 Assert.assertTrue("El siniestro se dictan� correctamente", mensajeDictaminacionExitosa.isDisplayed());
		   
		 WebElement cerrarMensajeDictaminacion = driver.findElement(By.xpath("//button[@class='close']"));
		 cerrarMensajeDictaminacion.click();
		   
		 WebElement nombreUsuario = driver.findElement(By.xpath("//div[@class='dropdown']//div[@class='d-flex align-items-center']"));
		 Thread.sleep(1000);
		 nombreUsuario.click();
		   
		 WebElement cerrarSesion = driver.findElement(By.xpath("//*[contains(text(), 'Cerrar Sesi�n')]"));
		 Thread.sleep(1000);
	     cerrarSesion.click();
		    

	}


}
