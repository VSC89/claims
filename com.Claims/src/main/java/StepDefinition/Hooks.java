package StepDefinition;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	
	private static ChromeDriver driver;
	private static int numberOfCase = 0; 
	
	@Before	
	public void setUp() {
		
		numberOfCase ++;
		System.out.println("Se est� ejecutando el escenario N�mero:" + numberOfCase);
		System.setProperty("webdriver.chrome.driver","./src/test/resources/Drivers/chromedriver.exe");		
		driver = new ChromeDriver();		
		driver.manage().window().maximize();		
		driver.get("http://54.151.70.166/");				
		
	}
	
	@After
	public void tearDown () {
		
		System.out.println("El escenario N�mero:  " + numberOfCase + " Se ejecut� correctamente");
		driver.close();
		driver.quit();		
		
	}
	
	public static ChromeDriver getDriver () {
		
		return driver;		
		
	}
	

}
