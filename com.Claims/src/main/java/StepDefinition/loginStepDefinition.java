package StepDefinition;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.chrome.ChromeDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class loginStepDefinition {
	
	private ChromeDriver driver = Hooks.getDriver();
	
	@Given("^Usuario Supervisor de la mesa de control$")
	public void usuario_Supervisor_de_la_mesa_de_control() throws Throwable {
        String titlePage = "Claims";	
		Assert.assertEquals(titlePage, driver.getTitle());
	}

	@When("^Ingreso la URL del sistema de siniestros$")
	public void ingreso_la_URL_del_sistema_de_siniestros() throws Throwable {
		
		
		
	}

	@Then("^Se me debe mostrar un boton para inicio de sesion$")
	public void se_me_debe_mostrar_un_para_inicio_de_sesion() throws Throwable {
		
		WebElement botonIngresarSmart = driver.findElement(By.xpath("//input[@class='btn btn-primary rounded-pill mx-auto d-block']"));
		//WebElement imageCardif = driver.findElement(By.className("py-5"));
		//Assert.assertTrue(imageCardif);
		Assert.assertTrue("Se cargo Correctamente la p�gina de Cardif", botonIngresarSmart.isDisplayed());
		//Assert.assertEquals("app-login", botonIngresarSmart.getTagName());
	    
	}

	@Then("^Presiono el boton de inicio de sesion$")
	public void presiono_el_boton_de_inicio_de_sesion() throws Throwable {
		WebElement botonIngresarSmart = driver.findElement(By.xpath("//input[@class='btn btn-primary rounded-pill mx-auto d-block']"));
	     
		botonIngresarSmart.click();
		Thread.sleep(1000);
		
		String perfilAdmin ="CONTROL_DESK";
		
		WebElement selectPerfil =  driver.findElement(By.xpath("//select[@class='form-control']"));
		selectPerfil.click();	
		
		Select listaPerfiles = new Select(selectPerfil);
		listaPerfiles.selectByVisibleText(perfilAdmin);
		
	}

	@Then("^El usuario accede correctamente al sistema Claims$")
	public void se_me_solicita_capturar_una_clave_personal_de_cinco_digitos() throws Throwable {
		
	    WebElement nombreUsuario = driver.findElement(By.xpath("//div[@class='dropdown']//div[@class='d-flex align-items-center']"));
	    Thread.sleep(2000);
	    nombreUsuario.click();
	}

	@Then("^Presiono el boton Ingresar$")
	public void presiono_el_boton_Ingresar() throws Throwable {
	    
		WebElement cerrarSesion = driver.findElement(By.xpath("//*[contains(text(), 'Cerrar Sesi�n')]"));
		Thread.sleep(2000);
		cerrarSesion.click();
	  
	}

	@Then("^Se muestra la pantalla Home$")
	public void se_muestra_la_pantalla_Home() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	
	

	
	
	
	
	

}
