package StepDefinition;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class AprobarSiniestro_MesaControl {
	
	private ChromeDriver driver = Hooks.getDriver();
	
	@Given("^Analista mesa de control, as$")
	public void analista_mesa_de_control_as() throws Throwable {
	
		WebElement botonIngresarSmart = driver.findElement(By.xpath("//input[@class='btn btn-primary rounded-pill mx-auto d-block']"));

		botonIngresarSmart.click();
		Thread.sleep(1000);

		String perfilMesaControl = "CONTROL_DESK";

		WebElement selectPerfil = driver.findElement(By.xpath("//select[@class='form-control']"));
		selectPerfil.click();

		Select listaPerfiles = new Select(selectPerfil);
		listaPerfiles.selectByVisibleText(perfilMesaControl);
	    
	    
	}

	@When("^Accedo al sistema Claims, as$")
	public void accedo_al_sistema_Claims_as() throws Throwable {
		
		Thread.sleep(1000);
		WebElement imageCardif = driver.findElement(By.xpath("//img[@class='pt-4 pb-4 pl-2 pr-2']"));

		Assert.assertTrue("Accedi� correctamente al Sistema Claims", imageCardif.isDisplayed());
	    
	}

	@Then("^Selecciono el siniestro que se aprobara, as$")
	public void selecciono_el_siniestro_que_se_aprobara_as() throws Throwable {
		
		WebElement checkBoxSiniestro = driver.findElement(By.xpath("/html/body/app-root/app-declarations-page/div/app-control-desk-declarations/table/tbody/tr[1]/td[1]/input"));
		checkBoxSiniestro.click();
		Thread.sleep(1000);	 
	    
	}

	@Then("^Se muestra el boton Aprobar, as$")
	public void se_muestra_el_boton_Aprobar_as() throws Throwable {
		
		Thread.sleep(3000);
		WebElement botonAprobar = driver.findElement(By.xpath("//button[contains(text(), ' Aprobar ')]"));

		Assert.assertTrue("Se mostr� correctamente el Bot�n Hit PIMUS", botonAprobar.isDisplayed());
	   
	}

	@When("^Doy clic en el boton Aprobar, as$")
	public void doy_clic_en_el_boton_Aprobar_as() throws Throwable {
		
		WebElement botonAprobar = driver.findElement(By.xpath("//button[contains(text(), ' Aprobar ')]"));
		botonAprobar.click();
		Thread.sleep(3000);
	   
	}

	@Then("^Se muestra el mensaje Estas seguro que deseas marcar el siniestro como aprobado, as$")
	public void se_muestra_el_mensaje_Estas_seguro_que_deseas_marcar_el_siniestro_como_aprobado_as() throws Throwable {
		
		WebElement mensajeConfirmacion = driver.findElement(By.xpath("//p[@class='text-center mt-3 mb-3']"));
	    Assert.assertTrue("Se mostro� correctamente el mensaje de confirmaci�n", mensajeConfirmacion.isDisplayed());	
	   
	}

	@When("^Doy clic en boton Continuar, as$")
	public void doy_clic_en_boton_Continuar_as() throws Throwable {
		
		WebElement botonContinuar = driver.findElement(By.xpath("//button[@class='btn btn-primary rounded-pill px-4 mr-3']"));
		botonContinuar.click();
		Thread.sleep(1000);			
	 	   
	}

	@Then("^Se muestra el mensaje de Actualizacion Exitosa, as$")
	public void se_muestra_el_mensaje_de_Actualizacion_Exitosa_as() throws Throwable {
		
		 WebElement mensajeDictaminacionExitosa = driver.findElement(By.xpath("//div[@role='alert']"));		
		 Assert.assertTrue("El siniestro se dictan� correctamente", mensajeDictaminacionExitosa.isDisplayed());
		   
		 WebElement cerrarMensajeDictaminacion = driver.findElement(By.xpath("//button[@class='close']"));
		 cerrarMensajeDictaminacion.click();
		   
		 WebElement nombreUsuario = driver.findElement(By.xpath("//div[@class='dropdown']//div[@class='d-flex align-items-center']"));
		 Thread.sleep(1000);
		 nombreUsuario.click();
		   
		 WebElement cerrarSesion = driver.findElement(By.xpath("//*[contains(text(), 'Cerrar Sesi�n')]"));
		 Thread.sleep(1000);
	     cerrarSesion.click();
	   
	}


}
