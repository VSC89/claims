package StepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DictaminacionProcedentePago {
	
	private ChromeDriver driver = Hooks.getDriver(); 
	
	
	@Given("^Usuario Dictaminador, pp$")
	public void usuario_Dictaminador_pp() throws Throwable {
		
		WebElement botonIngresarSmart = driver.findElement(By.xpath("//input[@class='btn btn-primary rounded-pill mx-auto d-block']"));	     
		botonIngresarSmart.click();
		Thread.sleep(1000);
		
		String perfilAdmin ="AUTHORIZER";
		
		WebElement selectPerfil =  driver.findElement(By.xpath("//select[@class='form-control']"));
		selectPerfil.click();	
		
		Select listaPerfiles = new Select(selectPerfil);
		listaPerfiles.selectByVisibleText(perfilAdmin);
	    
	}

	@When("^Accedo al sistema Claims, pp$")
	public void accedo_al_sistema_Claims_pp() throws Throwable {
		
		Thread.sleep(1000);
		WebElement imageCardif = driver.findElement(By.xpath("//img[@class='pt-4 pb-4 pl-2 pr-2']"));
		
		Assert.assertTrue("Accedi� correctamente al Sistema Claims", imageCardif.isDisplayed());
	    
	}

	@Then("^Selecciono el siniestro a Dictaminar, pp$")
	public void selecciono_el_siniestro_a_Dictaminar_pp() throws Throwable {
		    
		    WebElement seleccionSiniestro = driver.findElement(By.xpath("//tr"));	    
		    seleccionSiniestro.click();
		    Thread.sleep(1000);
		 
	}

	@Then("^Se muestra el detalle del siniestro y el boton dictaminar, pp$")
	public void se_muestra_el_detalle_del_siniestro_y_el_boton_dictaminar_pp() throws Throwable {
		   
		   WebElement detalleSiniestro = driver.findElement(By.xpath("//h3[contains(text(), 'Socio')]"));
		   Assert.assertTrue("Se mostro� correctamente el detalle del Siniestro", detalleSiniestro.isDisplayed());
	}

	@When("^Doy clic en el boton Dictaminar, pp$")
	public void doy_clic_en_el_boton_Dictaminar_pp() throws Throwable {
		   
		   JavascriptExecutor js = (JavascriptExecutor) driver;
		   js.executeScript("window.scrollBy (0,1200)");
		   Thread.sleep(2000);
		   WebElement botonDictaminar = driver.findElement(By.xpath("//button[@class='btn btn-primary rounded-pill px-5']"));
		   botonDictaminar.click();	   
	}

	@Then("^Selecciono la opcion Procedente de Pago, pp$")
	public void selecciono_la_opcion_Procedente_de_Pago_pp() throws Throwable {
		  
		   WebElement bottonProcedentePago = driver.findElement(By.xpath("//button[contains(text(),' Procedente pago ')]"));
		   bottonProcedentePago.click();
		   Thread.sleep(1000); 		 
	    
	}

	@Then("^Se muestra el mensaje de confirmacion, pp$")
	public void se_muestra_el_mensaje_de_confirmacion_pp() throws Throwable {
	    
		   WebElement mensajeConfirmacion = driver.findElement(By.xpath("//p[@class='text-center mt-3 mb-3']"));
		   Assert.assertTrue("Se mostro� correctamente el mensaje de confirmaci�n", mensajeConfirmacion.isDisplayed());
		   	   
		
	}

	@When("^Doy clic en el boton Continuar, pp$")
	public void doy_clic_en_el_boton_Continuar_pp() throws Throwable {
		
		WebElement botonContinuar = driver.findElement(By.xpath("//button[@class='btn btn-primary rounded-pill px-4 mr-3']"));
		botonContinuar.click();
		Thread.sleep(1000);
			 
	    
	}

	@When("^Se muestra el mensaje Dictaminacion Exitosa, pp$")
	public void se_muestra_el_mensaje_Dictaminacion_Exitosa_pp() throws Throwable {
		 WebElement mensajeDictaminacionExitosa = driver.findElement(By.xpath("//div[@role='alert']"));		
		   Assert.assertTrue("El siniestro se dictan� correctamente", mensajeDictaminacionExitosa.isDisplayed());
		   
		   WebElement cerrarMensajeDictaminacion = driver.findElement(By.xpath("//button[@class='close']"));
		   cerrarMensajeDictaminacion.click();
		   
		   WebElement nombreUsuario = driver.findElement(By.xpath("//div[@class='dropdown']//div[@class='d-flex align-items-center']"));
		    Thread.sleep(1000);
		    nombreUsuario.click();
		   
		   WebElement cerrarSesion = driver.findElement(By.xpath("//*[contains(text(), 'Cerrar Sesi�n')]"));
			Thread.sleep(1000);
			cerrarSesion.click();
		    
	}

	
	
	

}
