package StepDefinition;

import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DictaminacionRechazar {
	
	private ChromeDriver driver = Hooks.getDriver(); 	
	
	@Given("^Usuario Dictaminador, rejection$")
	public void usuario_Dictaminador_rejection() throws Throwable {
		
		WebElement botonIngresarSmart = driver.findElement(By.xpath("//input[@class='btn btn-primary rounded-pill mx-auto d-block']"));
	     
		botonIngresarSmart.click();
		Thread.sleep(1000);
		
		String perfilAdmin ="AUTHORIZER";
		
		WebElement selectPerfil =  driver.findElement(By.xpath("//select[@class='form-control']"));
		selectPerfil.click();	
		
		Select listaPerfiles = new Select(selectPerfil);
		listaPerfiles.selectByVisibleText(perfilAdmin);
	
	}

	@When("^Accedo al sistema Claims, rejection$")
	public void accedo_al_sistema_Claims_rejection() throws Throwable {
		
		Thread.sleep(1000);
		WebElement imageCardif = driver.findElement(By.xpath("//img[@class='pt-4 pb-4 pl-2 pr-2']"));		
		Assert.assertTrue("Accedi� correctamente al Sistema Claims", imageCardif.isDisplayed());
	   
	}

	@Then("^Selecciono el siniestro a Dictaminar, rejection$")
	public void selecciono_el_siniestro_a_Dictaminar_rejection() throws Throwable {
	   
		WebElement seleccionSiniestro = driver.findElement(By.xpath("//tr"));	    
	    seleccionSiniestro.click();
	    Thread.sleep(1000);
	   
	}

	@Then("^Se muestra el detalle del siniestro y el boton dictaminar, rejection$")
	public void se_muestra_el_detalle_del_siniestro_y_el_boton_dictaminar_rejection() throws Throwable {
	   
		WebElement detalleSiniestro = driver.findElement(By.xpath("//h3[contains(text(), 'Socio')]"));
		Assert.assertTrue("Se mostro� correctamente el detalle del Siniestro", detalleSiniestro.isDisplayed());
	   
	}

	@When("^Doy clic en el boton Dictaminar, rejection$")
	public void doy_clic_en_el_boton_Dictaminar_rejection() throws Throwable {
	    
		   JavascriptExecutor js = (JavascriptExecutor) driver;
		   js.executeScript("window.scrollBy (0,1200)");
		   Thread.sleep(2000);
		   WebElement botonDictaminar = driver.findElement(By.xpath("//button[@class='btn btn-primary rounded-pill px-5']"));
		   botonDictaminar.click();	   
	   
	}

	@Then("^Selecciono la opcion rechazar, rejection$")
	public void selecciono_la_opcion_rechazar_rejection() throws Throwable {
		
		 WebElement botonRechazar = driver.findElement(By.xpath("//button[contains(text(),' Rechazar ')]"));
		 botonRechazar.click();
		 Thread.sleep(2000); 
	
	}

	@Then("^Se muestran los motivos de rechazo, rejection$")
	public void se_muestran_los_motivos_de_rechazo_rejection() throws Throwable {

        List <WebElement> listaMotivosRechazo = driver.findElements(By.xpath("//label[@class='form-check-label']"));		
		for (int i = 1; i <= listaMotivosRechazo.size(); i++ ) 
		{
			WebElement motivoRechazo = driver.findElement(By.xpath("//label[@class='form-check-label' and @for='reason-5']"));
			motivoRechazo.click();
			Thread.sleep(1000);
			
		}
	
	}

	@When("^Doy clic en el boton Continuar, rejection$")
	public void doy_clic_en_el_boton_Continuar_rejection() throws Throwable {
	
		WebElement botonContinuar = driver.findElement(By.xpath("//button[@class='btn btn-primary rounded-pill px-4 mr-3']"));
		botonContinuar.click();
		Thread.sleep(1000);
	}

	@Then("^Se muestra el mensaje Dictaminacion Exitosa, rejection$")
	public void se_muestra_el_mensaje_Dictaminacion_Exitosa_rejection() throws Throwable {
	
		WebElement mensajeDictaminacionExitosa = driver.findElement(By.xpath("//div[@role='alert']"));		
		Assert.assertTrue("El siniestro se dictan� correctamente", mensajeDictaminacionExitosa.isDisplayed());
		   
		WebElement cerrarMensajeDictaminacion = driver.findElement(By.xpath("//button[@class='close']"));
		cerrarMensajeDictaminacion.click();
		   
		WebElement nombreUsuario = driver.findElement(By.xpath("//div[@class='dropdown']//div[@class='d-flex align-items-center']"));
		Thread.sleep(2000);
		nombreUsuario.click();
		   
		WebElement cerrarSesion = driver.findElement(By.xpath("//*[contains(text(), 'Cerrar Sesi�n')]"));
		Thread.sleep(2000);
		cerrarSesion.click();
		
	}

}
