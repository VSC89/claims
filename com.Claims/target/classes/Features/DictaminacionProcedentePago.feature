
Feature: Usuario Dictaminador requiere marcar como Procedente de Pago un siniestro

  
  Scenario: Dictaminar Procedente de Pago un Siniestro
  
    Given Usuario Dictaminador, pp
    When Accedo al sistema Claims, pp 
    Then Selecciono el siniestro a Dictaminar, pp
    Then Se muestra el detalle del siniestro y el boton dictaminar, pp
    When Doy clic en el boton Dictaminar, pp
    Then Selecciono la opcion Procedente de Pago, pp
    Then Se muestra el mensaje de confirmacion, pp
    When Doy clic en el boton Continuar, pp
    When Se muestra el mensaje Dictaminacion Exitosa, pp
    
    

 